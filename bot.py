# Для работы с ОС и временем
import os
import time
from datetime import datetime
import shutil
# Для работы со скачиванием и ошибками доступа 
import wget
import urllib.request
import urllib.error
# Для архивирования файлов
from zipfile import ZipFile, ZIP_DEFLATED
# API бота
import telebot;
from telebot import types

#----Получаем токен из фала----#
# os.chdir('pyclicktodownload')
fileTokens = open("token.txt", "r")
token = fileTokens.readlines()
bot = telebot.TeleBot(token[0]);

#-------Глобальные переменные---------#
inZips = False              # Находимся ли в каталоге архивов
inDownloads = False         # Находимся ли в каталоге загрузок
canDeleteFile = False       # Разрешено ли удалять файлы
listFiles = []              # Список файлов в каталоге
link = ''                   # Ссылка для скачивания файла
#---Только эти файлы должны быть в главном каталоге---#
needMainDirFiles = ['bot.py', 'CHANGELOG.md', 'downloads', 'logs', 'requirements.txt', 'zips', 'whitelist.txt', 'README.md', 'token.txt', '.git', 'images']

fileWhiteList = open("whitelist.txt", "r")
whiteList = fileWhiteList.readlines()

def SendMessages(typeMessage, nameFile = ''):
    """
    Функция возвращает определенный тип сообщения пользователю
    Здесь прописываются все сообщения, чтобы не засорять код
    """

    messageList = ["""👋 Привет! Я твой бот-помошник для скачивания 
определенных файлов из сети интернет посредством телеграм!👨‍💻
Отправь мне Прямую ссылку на файл и я 
сохраню файл к себе, а затем отправлю тебе🎁
    Не забудь посмотреть инструкцию по кнопке!""",
                   'Нельзя скачать файл. Отказано в доступе.❌',
                   'Нет файлов для архивирования или загрузки❌',
                   'Приступаю к загрузке. Подождите, пока я его не скачаю👓',
                   'Приступаю к архивированию файла. Подождите немного👓',
                   'Файл "',
                   '" скачан✅',
                   '" заархивирован✅',
                   'Вы перешли в главное меню!🌎',
                   'Вы уверены, что это рабочая и правильная ссылка?🤔',
                   'Отправьте мне ссылку на скачивание файла',
                   'Вот список файлов, доступных для скачивания:            ',
                   'Неизвестная команда❌',
                   'Напишите номер файла, необходимого для отправки:',
                   'Напишите номер файла, необходимого для архивирования:',
                   'Напишите файлы в отрезке по примеру [1-10] для выбора с 1 по 10',
                   'Напишите номер файла для удаления',
                   'Файл(ы) удален(ы)🗑️',
                   'Ссылка⛓️',
                   'Файлы📙',
                   'Архивы📘',
                   'Удалить файл🗑️',
                   'Главное меню🌎',
                   'Нет❌',
                   'Да✅',
                   'Неправильная ссылка❌',
                   'Такой файл уже существует🤔',
                   'Вот список файлов, доступных для архивирования:',
                   'Неверная нумерация❌',
                   'Отправляю файл👨‍💻',
                   'Отправляю файлы👨‍💻',
                   'Отправка закончена✅',
                   'Нет файлов для удаления❌',
                   'Инструкция🤯',
                   'Свободная память💿',
                   'Не хватает памяти для архивации❌💿']

    match typeMessage:
        case 'startMessage': return messageList[0]
        case 'errorDownloadMessage': return messageList[1]
        case 'notFoundFilesMessage': return messageList[2]
        case 'startDownloadMessage': return messageList[3]
        case 'startArchiveMessage': return messageList[4]
        case 'endDownloadMessage': return messageList[5] + nameFile + messageList[6]
        case 'endArchiveMessage': return messageList[5] + nameFile + messageList[7]
        case 'startMenu': return messageList[8]
        case 'checkLink': return messageList[9]
        case 'takeMeLink': return messageList[10]
        case 'archiveList': return messageList[11]
        case 'errorComand': return messageList[12]
        case 'numberFileToSend': return messageList[13]
        case 'numberFileToArchive': return messageList[14]
        case 'numberFileToSegment': return messageList[15]
        case 'numberFileToDel': return messageList[16]
        case 'deleteMessage': return messageList[17]
        case 'link': return messageList[18]
        case 'file': return messageList[19]
        case 'archive': return messageList[20]
        case 'delete': return messageList[21]
        case 'menu': return messageList[22]
        case 'no': return messageList[23]
        case 'yes': return messageList[24]
        case 'errorLink': return messageList[25]
        case 'secondFile': return messageList[26]
        case 'fileList': return messageList[27]
        case 'errorNum': return messageList[28]
        case 'sendOneFile': return messageList[29]
        case 'sendMoreFiles': return messageList[30]
        case 'endSendFiles': return messageList[31]
        case 'notDelFiles': return messageList[32]
        case 'guide': return messageList[33]
        case 'totalRom': return messageList[34]
        case 'notFreeRom': return messageList[35]

def SplitFile(f_name, p_size):
    """
    Функция, делящая архив на равные части
    """
    BS = 512            # размер блока которыми читаем из файла
    cur_vol = 1         # текущий номер тома (нужен для подписи архива)
    written = 0         # сколько байт записали

    with open(f_name, 'rb') as src:
        while True:
            output_fname = '{}.{}'.format(f_name, str(cur_vol).zfill(3))
            output = open(output_fname, 'wb')
            while written < p_size:
                data = src.read(BS)
                if data == b'':
                    break
                output.write(data)
                written+=len(data)
            else:
                output.close()
                os.rename(output_fname,os.path.join('zips',output_fname))
                cur_vol += 1
                written = 0
                continue
            output.close()
            os.rename(output_fname,os.path.join('zips',output_fname))
            break

def DeleteFiles(a, id, b = 0):
        global listFiles
        if inZips:
            directory = 'zips/'
        else:
            directory = 'downloads/'

        if b != 0:
            for i in range(a, b+1):
                if inZips:
                    directory = 'zips/'
                else:
                    directory = 'downloads/'
                directory += listFiles[i-1]
                os.remove(directory)
                LogWrite('Удалил файл '+ listFiles[a-1], id)

        else:
            directory += listFiles[a-1]
            os.remove(directory)
            LogWrite('Удалил файл '+ listFiles[a-1], id)

        if inZips:
            directory = 'zips/'
        else:
            directory = 'downloads/'
        listFiles = os.listdir(directory)

# def WhoAmI(id):
#     """
#     Резервирование имён через user id
#     """
#     match id:
#         case 774205754: return 'TheVeonMain'
#         case _: return id

def LogWrite(message, id):
    """
    Логирование действий пользователя
    """
    directory = 'logs/'
    fileName = id

    current_datetime = str(datetime.now())

    logFile = open(directory+str(fileName)+'.txt', 'a', encoding="utf-8")
    logFile.write(current_datetime + ' ' + message+'\n')
    logFile.close()

def FreeRom(Flag = False):
    """
    Отображение свободной памяти
    Также получение просто свободной памяти для сравнения
    """
    result = str(shutil.disk_usage('C:/'))
    result = result.replace(')', '')
    result = result.split('free=')
    result = int(result[1])
    
    if Flag:
        return result
    elif result < 1073741824:
        return 'Свободной памяти: ' + str(result // 1048576) + ' МБайт'
    return 'Свободной памяти: ' + str(result // 1073741824) + ' ГБайт'

@bot.message_handler(commands=['start'])
def start(message):
    """
    Функция, работающая при команде /start
    """
    if str(message.from_user.id) in whiteList:
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        btn1 = types.KeyboardButton(SendMessages('link'))
        btn2 = types.KeyboardButton(SendMessages('file'))
        btn3 = types.KeyboardButton(SendMessages('archive'))
        btn4 = types.KeyboardButton(SendMessages('guide'))
        btn5 = types.KeyboardButton(SendMessages('totalRom'))
        markup.add(btn1, btn2, btn3, btn4, btn5)
        bot.send_message(message.from_user.id, SendMessages('startMessage'), reply_markup=markup)
        LogWrite('Прописал /start '+message.text, message.from_user.id)
    else:
        LogWrite('Прописал /start '+message.text, message.from_user.id)

@bot.message_handler(content_types=['text'])
def start_menu(message):

    if str(message.from_user.id) in whiteList:
    
        global inZips, inDownloads, canDeleteFile, listFiles, link, FlagAdmin

        #---Проверка на ввод одного числа в сообщении---#
        try:
            intInput = int(message.text)
        except ValueError:
            intInput = -1

        if message.text == SendMessages('link') or message.text == SendMessages('no'):
            #-------Запрос ссылки от пользователя-------#
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            btn1 = types.KeyboardButton(SendMessages('menu'))
            markup.add(btn1)
            bot.send_message(message.from_user.id, SendMessages('takeMeLink'), reply_markup=markup)
            LogWrite('Нажал на кнопку ссылка '+message.text, message.from_user.id)

        elif message.text[0:4] == 'http':
            #------Если пользователь ввёл ссылку------#
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            link = message.text
            btn1 = types.KeyboardButton(SendMessages('yes'))
            btn2 = types.KeyboardButton(SendMessages('no'))
            markup.add(btn1, btn2)
            bot.send_message(message.from_user.id, SendMessages('checkLink'), reply_markup=markup)
            LogWrite('Ввёл ссылку '+message.text, message.from_user.id)

        elif message.text == SendMessages('yes'):
            #------Пользователь подтвердил правильность ссылки------#
            directory = 'downloads'
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            filelist = os.listdir(directory)
            filelistsec = os.listdir('zips')
            #--Попытка скачать файл--#
            try:
                LogWrite('Начинаем загрузку', message.from_user.id)
                bot.send_message(message.from_user.id, SendMessages('startDownloadMessage'), reply_markup=markup)
                filename = wget.download(link)
                LogWrite('Скачался файл '+filename, message.from_user.id)
                file_info = os.stat(filename)
                file_size = file_info.st_size
                if filename in filelist or filename in filelistsec:
                    #---Если такой файл уже существует---#
                    bot.send_message(message.from_user.id, SendMessages('secondFile', filename), reply_markup=markup)
                    LogWrite('Повторный файл '+filename+' удален', message.from_user.id)
                    os.remove(filename)
                elif filename[-4:-1]+filename[-1] == '.zip' and file_size <= 51380224:
                    #---Если это архив и он весит меньше 50 МБайт---#
                    LogWrite('Переместился в zips '+filename, message.from_user.id)
                    os.rename(filename,os.path.join('zips',filename))
                    btn1 = types.KeyboardButton(SendMessages('menu'))
                    markup.add(btn1)
                    bot.send_message(message.from_user.id, SendMessages('endDownloadMessage', filename), reply_markup=markup)                
                else:
                    LogWrite('Переместился в downloads '+filename, message.from_user.id)
                    os.rename(filename,os.path.join(directory,filename))
                    btn1 = types.KeyboardButton(SendMessages('menu'))
                    markup.add(btn1)
                    bot.send_message(message.from_user.id, SendMessages('endDownloadMessage', filename), reply_markup=markup)
            #--Если ошибка в ссылке--#
            except ValueError:
                btn1 = types.KeyboardButton(SendMessages('menu'))
                markup.add(btn1)
                bot.send_message(message.from_user.id, SendMessages('errorLink'), reply_markup=markup)
                LogWrite('Неверная ссылка '+message.text, message.from_user.id)
            #--Eсли нет доступа к файлу--#
            except urllib.error.HTTPError as err:
                if 0 <= err.code:
                    btn1 = types.KeyboardButton(SendMessages('menu'))
                    markup.add(btn1)
                    bot.send_message(message.from_user.id, SendMessages('errorDownloadMessage'), reply_markup=markup)
                    LogWrite('Файл не скачался из-за доступа '+message.text, message.from_user.id)
            except urllib.error.URLError as err:
                if 'error' in str(err) and '11001' in str(err):
                    btn1 = types.KeyboardButton(SendMessages('menu'))
                    markup.add(btn1)
                    bot.send_message(message.from_user.id, SendMessages('errorDownloadMessage'), reply_markup=markup)
                    LogWrite('Ошибка сокета '+message.text, message.from_user.id)
            except OSError:
                    btn1 = types.KeyboardButton(SendMessages('menu'))
                    markup.add(btn1)
                    bot.send_message(message.from_user.id, SendMessages('errorDownloadMessage'), reply_markup=markup)
                    LogWrite('Ошибка сокета '+message.text, message.from_user.id)

        elif message.text == SendMessages('file'):
            #----Если пользователь зашёл в файлы----#
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            btn1 = types.KeyboardButton(SendMessages('delete'))
            btn2 = types.KeyboardButton(SendMessages('menu'))
            markup.add(btn1, btn2)

            inDownloads = True
            inZips = False
            listFiles = os.listdir('downloads')
        
            if listFiles != []:
                #--Если есть файлы, то вывести их--#
                string = SendMessages('fileList')
                bot.send_message(message.from_user.id, string, reply_markup=markup)
                count = 1
                for stri in listFiles:
                    file_info = os.stat('downloads/'+stri)
                    file_size = file_info.st_size

                    if file_size < 1024:
                        file_size = ' - ' + str(file_size) + ' Байт'
                    elif file_size < 1048576:
                        file_size = ' - ' + str(file_size // 1024) + ' КБайт'
                    elif file_size < 1073741824:
                        file_size = ' - ' + str(file_size // 1048576) + ' МБайт'
                    else:
                        file_size = ' - ' + str(file_size // 1073741824) + ' ГБайт'

                    string = str(count) + ': ' + str(stri) + file_size
                    count += 1
                    bot.send_message(message.from_user.id, string, reply_markup=markup)
                    time.sleep(0.8)
                bot.send_message(message.from_user.id, SendMessages('numberFileToArchive'), reply_markup=markup)
                LogWrite('Получил список файлов '+message.text, message.from_user.id)
            else:
                bot.send_message(message.from_user.id, SendMessages('notFoundFilesMessage'), reply_markup=markup)
                LogWrite('Файлы отсутствуют '+message.text, message.from_user.id)

        elif message.text == SendMessages('archive'):
            #---Если пользователь зашел в архивы---#
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            btn1 = types.KeyboardButton(SendMessages('delete'))
            btn2 = types.KeyboardButton(SendMessages('menu'))
            markup.add(btn1, btn2)

            inZips = True
            inDownloads = False
            listFiles = os.listdir('zips')
        
            if listFiles != []:
                string = SendMessages('archiveList')
                bot.send_message(message.from_user.id, string, reply_markup=markup)
                count = 1
                for stri in listFiles:
                    string = str(count) + ': ' + str(stri)
                    count += 1
                    bot.send_message(message.from_user.id, string, reply_markup=markup)
                    time.sleep(0.8)
                LogWrite('Получил список архивов '+message.text, message.from_user.id)
                bot.send_message(message.from_user.id, SendMessages('numberFileToSend'), reply_markup=markup)
                bot.send_message(message.from_user.id, SendMessages('numberFileToSegment'), reply_markup=markup)
            else:
                bot.send_message(message.from_user.id, SendMessages('notFoundFilesMessage'), reply_markup=markup)
            LogWrite('Зашёл в архивы '+message.text, message.from_user.id)

        elif message.text == SendMessages('menu'):
            #----Переход в главное меню----#
            inZips = False
            inDownloads = False
            canDeleteFile = False

            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            btn1 = types.KeyboardButton(SendMessages('link'))
            btn2 = types.KeyboardButton(SendMessages('file'))
            btn3 = types.KeyboardButton(SendMessages('archive'))
            btn4 = types.KeyboardButton(SendMessages('guide'))
            btn5 = types.KeyboardButton(SendMessages('totalRom'))
            markup.add(btn1, btn2, btn3, btn4, btn5)
            bot.send_message(message.from_user.id, SendMessages('startMenu'), reply_markup=markup)
            LogWrite('Вернулся в главное меню '+message.text, message.from_user.id)

        elif message.text == SendMessages('delete') and listFiles != []:
            #----Сообщение об удалении файла----#
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            canDeleteFile = True
            btn1 = types.KeyboardButton(SendMessages('menu'))
            markup.add(btn1)
            bot.send_message(message.from_user.id, SendMessages('numberFileToDel'), reply_markup=markup)
            bot.send_message(message.from_user.id, SendMessages('numberFileToSegment'), reply_markup=markup)
            LogWrite('Нажал на кнопку удалить файл '+message.text, message.from_user.id)

        elif canDeleteFile and ((intInput >= 1) or ('[' in message.text and ']' in message.text)):
            #---Удаление файлов---#
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            canDeleteFile = False

            btn1 = types.KeyboardButton(SendMessages('menu'))
            markup.add(btn1)

            if intInput >= 1:
                DeleteFiles(intInput, message.from_user.id)
                bot.send_message(message.from_user.id, SendMessages('deleteMessage'), reply_markup=markup)
            elif ('[' in message.text and ']' in message.text):
                interval = message.text
                interval = interval.replace('[', '')
                interval = interval.replace(']', '')
                try:
                    a, b = [int(i) for i in interval.split('-')]
                    if (1 <= a < b <= len(listFiles)):
                        DeleteFiles(a, message.from_user.id, b)
                        bot.send_message(message.from_user.id, SendMessages('deleteMessage'), reply_markup=markup)
                        LogWrite('Пользователь удалил файл(ы) '+message.text, message.from_user.id)
                    else:
                        bot.send_message(message.from_user.id, SendMessages('errorNum'), reply_markup=markup)
                        LogWrite('Пользователь ошибся с нумирацией '+message.text, message.from_user.id)
                except ValueError:
                    bot.send_message(message.from_user.id, SendMessages('errorNum'), reply_markup=markup)
                    LogWrite('Пользователь ошибся с нумирацией '+message.text, message.from_user.id)
                except TypeError:
                    bot.send_message(message.from_user.id, SendMessages('errorNum'), reply_markup=markup)
                    LogWrite('Пользователь ошибся с нумирацией '+message.text, message.from_user.id)
        
            else:
                bot.send_message(message.from_user.id, SendMessages('errorNum'), reply_markup=markup)
                LogWrite('Пользователь ошибся с нумирацией '+message.text, message.from_user.id)

        elif (intInput >= 1) and inDownloads:
            #---Пользователь архивирует файл---#
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            zipList = os.listdir('zips')

            if (1 <=intInput <= len(listFiles)):

                file_info = os.stat('downloads/'+listFiles[intInput-1])
                file_size = file_info.st_size

                if (listFiles[intInput-1] + '.zip' in zipList) or (listFiles[intInput-1] + '.zip.001' in zipList) or (listFiles[intInput-1] in zipList) or (listFiles[intInput-1] + '.001' in zipList):
                    #---Если уже есть такой файл---#
                    bot.send_message(message.from_user.id, SendMessages('secondFile'), reply_markup=markup)
                    LogWrite('Ошибка в архивировании повторного файла '+ message.text, message.from_user.id)
                
                elif file_size*2 > FreeRom(True):
                    btn3 = types.KeyboardButton(SendMessages('menu'))
                    markup.add(btn3)
                    bot.send_message(message.from_user.id, SendMessages('notFreeRom'), reply_markup=markup)
                    LogWrite('Архивирование недоступно из-за памяти', message.from_user.id)

                else:
                    #--Если пользователь ввёл правильно нумерацию--#
                    directory = 'downloads/'
                    directory += listFiles[intInput-1]
                    zipname = '' + listFiles[intInput-1]
    ########################################################################
    #       Этот участок кода надо переделать без копирования файла     #### 
                    if '.zip' not in str(listFiles[intInput-1]):        ####
                        zipname += '.zip'                               ####
    ########################################################################
                    bot.send_message(message.from_user.id, SendMessages('startArchiveMessage'), reply_markup=markup)
                    LogWrite('Начало архивирования '+ message.text, message.from_user.id)

                    with ZipFile(zipname, "w", compression=ZIP_DEFLATED, compresslevel=5) as myzip:
                        myzip.write(directory)

                    zipName = zipname
                    file_info = os.stat(zipname)
                    file_size = file_info.st_size
                    if file_size <= 51380224:
                        LogWrite('Создан архив '+ zipname+ ' и перемещен в zips', message.from_user.id)
                        os.rename(zipname,os.path.join('zips',zipname))
                    else:
                        SplitFile(zipname, 51380224)
                        LogWrite('Разделен архив '+ zipname, message.from_user.id)
                        os.remove(zipname)
                        LogWrite('Удален архив', message.from_user.id)
                    btn3 = types.KeyboardButton(SendMessages('menu'))
                    markup.add(btn3)
                    bot.send_message(message.from_user.id, SendMessages('endArchiveMessage', zipName), reply_markup=markup)
                    LogWrite('Архивирование закончено', message.from_user.id)

            else:
                bot.send_message(message.from_user.id, SendMessages('errorNum'), reply_markup=markup)
                LogWrite('Пользователь ошибся с нумирацией '+message.text, message.from_user.id)

        elif inZips and (intInput >= 1 or ('[' in message.text and ']' in message.text)) and listFiles != []:
            #---Отправляем пользователю файлы из каталога zips---#
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            btn1 = types.KeyboardButton(SendMessages('menu'))
            markup.add(btn1)

            if len(listFiles) >= intInput >= 1:
                filedirectory = 'zips/'
                filedirectory += listFiles[intInput-1]

                bot.send_message(message.from_user.id, SendMessages('sendOneFile'), reply_markup=markup)
                bot.send_document(message.from_user.id, open(filedirectory, 'rb'), reply_markup=markup)
                LogWrite('Пользователю отправлен файл '+filedirectory, message.from_user.id)
                bot.send_message(message.from_user.id, SendMessages('endSendFiles'), reply_markup=markup)
            elif ('[' in message.text and ']' in message.text):
                interval = message.text
                interval = interval.replace('[', '')
                interval = interval.replace(']', '')
                try:
                    a, b = [int(i) for i in interval.split('-')]

                    if (1 <= a < b <= len(listFiles)) and (a != None and b != None):
                        bot.send_message(message.from_user.id, SendMessages('sendMoreFiles'), reply_markup=markup)
                        for i in range(a, b+1):
                            filedirectory = 'zips/'
                            filedirectory += listFiles[i-1]
                            bot.send_document(message.from_user.id, open(filedirectory, 'rb'), reply_markup=markup)
                            LogWrite('Пользователю отправлен файл '+filedirectory, message.from_user.id)
                            time.sleep(5)
                        bot.send_message(message.from_user.id, SendMessages('endSendFiles'), reply_markup=markup)
                        LogWrite('Пользователю отправлены файлы '+message.text, message.from_user.id)
                    else:
                        bot.send_message(message.from_user.id, SendMessages('errorNum'), reply_markup=markup)
                        LogWrite('Пользователь ошибся с нумирацией '+message.text, message.from_user.id)
                except ValueError:
                    a = None
                    b = None
                    bot.send_message(message.from_user.id, SendMessages('errorNum'), reply_markup=markup)
                    LogWrite('Пользователь ошибся с нумирацией '+message.text, message.from_user.id)
                except TypeError:
                    bot.send_message(message.from_user.id, SendMessages('errorNum'), reply_markup=markup)
                    LogWrite('Пользователь ошибся с нумирацией '+message.text, message.from_user.id)
            else:
                bot.send_message(message.from_user.id, SendMessages('errorNum'), reply_markup=markup)
                LogWrite('Пользователь ошибся с нумирацией '+message.text, message.from_user.id)

        elif listFiles == [] and (inZips or inDownloads):
            #---Если пользователь хочет удалить файлы, которых нет---#
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            btn1 = types.KeyboardButton(SendMessages('menu'))
            markup.add(btn1)
            bot.send_message(message.from_user.id, SendMessages('notDelFiles'), reply_markup=markup)
            LogWrite('Отсутствуют файлы для удаления '+message.text, message.from_user.id)

        elif message.text == SendMessages('guide'):
            #---Просмотр гайда---#
            bot.send_message(message.from_user.id, 'Прочитать инструкцию Вы можете по ' + '[ссылке](https://telegra.ph/Instrukciya-dlya-Umnikov-09-09)' , parse_mode='Markdown')
            LogWrite('Получил гайд '+message.text, message.from_user.id)        

        elif message.text == SendMessages('totalRom'):
            #------Отображение свободной памяти------#
            bot.send_message(message.from_user.id, FreeRom(), parse_mode='Markdown')
            LogWrite('Пользователь посмотрет свободную мапять '+message.text, message.from_user.id)

        elif '///' in message.text or 'localhost' in message.text or message.text[0:5] in ['file', 'open']:
            #------Попытка ввода скрипта юзером------#
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            btn1 = types.KeyboardButton(SendMessages('menu'))
            markup.add(btn1)
            bot.send_message(message.from_user.id, SendMessages('errorComand'), reply_markup=markup)
            LogWrite('Пользователь ввёл скрипт '+message.text, message.from_user.id)

        else:
            #------Выполнение незадуманной команды------#
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
            btn1 = types.KeyboardButton(SendMessages('menu'))
            markup.add(btn1)
            bot.send_message(message.from_user.id, SendMessages('errorComand'), reply_markup=markup)
            LogWrite('Пользователь ввёл бред '+message.text, message.from_user.id)

        #---Удаление всех лишних файлов в каталоге---#
        mainDirFiles = os.listdir()
        for file in mainDirFiles:
            if file not in needMainDirFiles:
                os.remove(file)

        time.sleep(0.8)
    
    else:
        LogWrite('Попытался что-то сделать '+message.text, message.from_user.id)

bot.polling(none_stop=True, interval=0)
