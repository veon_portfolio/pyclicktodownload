<h1 align="center">PyClickToDownload</h1>

<p align="center">
	<img src="/images/pyclicktodownload.png?cachefix" />
</p>

### Описание[RU]:
Данный код представляет собой основу для Телеграм бота, принимающий ПРЯМУЮ ссылку на конечный файл,
затем скачивающий себе. Пользователь выбирает скачанные файлы и архивирует внутри бота, а затем решает, какие файлы ему отправить. 
Архивирование производится на дробные части. Открывать архивы необходимо архиватором, поддерживающим дробные архивы. 

### Description[EN]:
This code is the basis for a Telegram bot that accepts a DIRECT link to the final file,
then downloading for himself. The user selects the downloaded files and archives them inside the bot, and then decides which files to send to them.
Archiving is done in fractional parts. Archives must be opened with an archiver that supports fractional archives.

### System requirements: 
- Python 3.11.6
- Python libraries
- - pyTelegramBotAPI==4.16.1
- - telebot==0.0.5
- - wget==3.2

### Как использовать[RU]:
Отправить боту КОНЕЧНУЮ ссылку на файл для поседующей её загрузки. Бот уведомит об окончании загрузки и перенесении её в каталог загрузки. По нажатию на кнопку "файлы" бот отобразит возможные для архивировани ресурсы. Выбрав файлы для архивирования, они перенесутся в соответствующий каталог, с последующей возможностью скачивания их из каталога "архивы". Выбрав необходимые файл для скачивания, бот вышлет их Вам поочереди
не более 50 МБ за один файл. 

### How to use[EN]:
Send the bot the DIRECT link to the file to download it. The bot will notify you that the download is complete and will be transferred to the download directory. By clicking on the “files” button, the bot will display the resources available for archiving. Having selected files for archiving, they will be transferred to the appropriate directory, with the subsequent ability to download them from the “archives” directory. Having selected the necessary files to download, the bot will send them to you one by one
no more than 50 MB per file.

### Info:
- Support: blastyboom2001@gmail.com
<br />
(c) TheVeon